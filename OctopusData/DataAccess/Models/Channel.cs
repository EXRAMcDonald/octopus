﻿namespace OctopusData.DataAccess.Models
{
    public class Channel
    {
        public string ChannelId { get; set; }
        public string ChannelName { get; set; }
    }
}
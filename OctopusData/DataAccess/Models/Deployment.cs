﻿using System;

namespace OctopusData.DataAccess.Models
{
    public class Deployment
    {
        public string DeploymentId { get; set; }
        public string DeploymentName { get; set; }
        public Project Project { get; set; }
        public Channel Channel { get; set; }
        public Models.Environment Environment { get; set; }
        public Release Release { get; set; }
        public string TaskId { get; set; }
        public string TaskState { get; set; }
        public DateTime Created { get; set; }
        public DateTime QueueTime { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime CompletedTime { get; set; }
        public int DurationSeconds { get; set; }
        public string DeployedBy { get; set; }
    }
}

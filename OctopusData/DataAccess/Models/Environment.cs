﻿namespace OctopusData.DataAccess.Models
{
    public class Environment
    {
        public string EnvironmentId { get; set; }
        public string EnvironmentName { get; set; }
    }
}
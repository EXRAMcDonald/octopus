﻿namespace OctopusData.DataAccess.Models
{
    public class Release
    {
        public string ReleaseId { get; set; }
        public string ReleaseVersion { get; set; }
    }
}
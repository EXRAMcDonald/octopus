﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace OctopusData.DataAccess.Contexts
{
    public class OctopusDeploymentContext : DbContext
    {
        public DbSet<Models.Deployment> Deployments { get; set; }
        public DbSet<Models.Project> Projects { get; set; }
        public DbSet<Models.Channel> Channels { get; set; }
        public DbSet<Models.Environment> Environments { get; set; }
        public DbSet<Models.Release> Releases { get; set; }
    }
}

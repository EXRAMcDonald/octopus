namespace OctopusData.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Project : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Projects",
                c => new
                    {
                        ProjectId = c.String(nullable: false, maxLength: 128),
                        ProjectName = c.String(),
                    })
                .PrimaryKey(t => t.ProjectId);
            
            AddColumn("dbo.Deployments", "ProjectId_ProjectId", c => c.String(maxLength: 128));
            CreateIndex("dbo.Deployments", "ProjectId_ProjectId");
            AddForeignKey("dbo.Deployments", "ProjectId_ProjectId", "dbo.Projects", "ProjectId");
            DropColumn("dbo.Deployments", "ProjectId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Deployments", "ProjectId", c => c.String());
            DropForeignKey("dbo.Deployments", "ProjectId_ProjectId", "dbo.Projects");
            DropIndex("dbo.Deployments", new[] { "ProjectId_ProjectId" });
            DropColumn("dbo.Deployments", "ProjectId_ProjectId");
            DropTable("dbo.Projects");
        }
    }
}

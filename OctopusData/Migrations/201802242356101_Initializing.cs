namespace OctopusData.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initializing : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Deployments",
                c => new
                    {
                        DeploymentId = c.String(nullable: false, maxLength: 128),
                        DeploymentName = c.String(),
                        ProjectId = c.String(),
                        ChannelId = c.String(),
                        EnvironmentId = c.String(),
                        ReleaseId = c.String(),
                        TaskId = c.String(),
                        TaskState = c.String(),
                        Created = c.DateTime(nullable: false),
                        QueueTime = c.DateTime(nullable: false),
                        StartTime = c.DateTime(nullable: false),
                        CompletedTime = c.DateTime(nullable: false),
                        DurationSeconds = c.Int(nullable: false),
                        DeployedBy = c.String(),
                    })
                .PrimaryKey(t => t.DeploymentId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Deployments");
        }
    }
}

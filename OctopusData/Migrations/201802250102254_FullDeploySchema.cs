namespace OctopusData.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FullDeploySchema : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Deployments", name: "ProjectId_ProjectId", newName: "Project_ProjectId");
            RenameIndex(table: "dbo.Deployments", name: "IX_ProjectId_ProjectId", newName: "IX_Project_ProjectId");
            CreateTable(
                "dbo.Channels",
                c => new
                    {
                        ChannelId = c.String(nullable: false, maxLength: 128),
                        ChannelName = c.String(),
                    })
                .PrimaryKey(t => t.ChannelId);
            
            CreateTable(
                "dbo.Environments",
                c => new
                    {
                        EnvironmentId = c.String(nullable: false, maxLength: 128),
                        EnvironmentName = c.String(),
                    })
                .PrimaryKey(t => t.EnvironmentId);
            
            CreateTable(
                "dbo.Releases",
                c => new
                    {
                        ReleaseId = c.String(nullable: false, maxLength: 128),
                        ReleaseVersion = c.String(),
                    })
                .PrimaryKey(t => t.ReleaseId);
            
            AddColumn("dbo.Deployments", "Channel_ChannelId", c => c.String(maxLength: 128));
            AddColumn("dbo.Deployments", "Environment_EnvironmentId", c => c.String(maxLength: 128));
            AddColumn("dbo.Deployments", "Release_ReleaseId", c => c.String(maxLength: 128));
            CreateIndex("dbo.Deployments", "Channel_ChannelId");
            CreateIndex("dbo.Deployments", "Environment_EnvironmentId");
            CreateIndex("dbo.Deployments", "Release_ReleaseId");
            AddForeignKey("dbo.Deployments", "Channel_ChannelId", "dbo.Channels", "ChannelId");
            AddForeignKey("dbo.Deployments", "Environment_EnvironmentId", "dbo.Environments", "EnvironmentId");
            AddForeignKey("dbo.Deployments", "Release_ReleaseId", "dbo.Releases", "ReleaseId");
            DropColumn("dbo.Deployments", "ChannelId");
            DropColumn("dbo.Deployments", "EnvironmentId");
            DropColumn("dbo.Deployments", "ReleaseId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Deployments", "ReleaseId", c => c.String());
            AddColumn("dbo.Deployments", "EnvironmentId", c => c.String());
            AddColumn("dbo.Deployments", "ChannelId", c => c.String());
            DropForeignKey("dbo.Deployments", "Release_ReleaseId", "dbo.Releases");
            DropForeignKey("dbo.Deployments", "Environment_EnvironmentId", "dbo.Environments");
            DropForeignKey("dbo.Deployments", "Channel_ChannelId", "dbo.Channels");
            DropIndex("dbo.Deployments", new[] { "Release_ReleaseId" });
            DropIndex("dbo.Deployments", new[] { "Environment_EnvironmentId" });
            DropIndex("dbo.Deployments", new[] { "Channel_ChannelId" });
            DropColumn("dbo.Deployments", "Release_ReleaseId");
            DropColumn("dbo.Deployments", "Environment_EnvironmentId");
            DropColumn("dbo.Deployments", "Channel_ChannelId");
            DropTable("dbo.Releases");
            DropTable("dbo.Environments");
            DropTable("dbo.Channels");
            RenameIndex(table: "dbo.Deployments", name: "IX_Project_ProjectId", newName: "IX_ProjectId_ProjectId");
            RenameColumn(table: "dbo.Deployments", name: "Project_ProjectId", newName: "ProjectId_ProjectId");
        }
    }
}
